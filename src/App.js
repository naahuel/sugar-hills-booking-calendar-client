import React, { Component } from 'react';
import Calendar from 'react-calendar';
import moment from 'moment-timezone';
import qs from 'qs';
import axios from 'axios';
import get from 'lodash/get';
import keys from 'lodash/keys';
import './App.css';

/**
 * This should work under WP
 * use 'http://localhost/sugar-hills/wp-admin/admin-ajax.php' for dev
 */
// const AJAX_URL = 'http://localhost/sugar-hills/wp-admin/admin-ajax.php';
const AJAX_URL = window.sh_calendar_localization.ajaxurl;

class App extends Component {
  constructor(props){
    super(props);

    /** Binding */
    this._handleCalendarChange = this._handleCalendarChange.bind(this);
    this._handleTimeChange     = this._handleTimeChange.bind(this);
    this._getMonthAvailability = this._getMonthAvailability.bind(this);
    this._renderAvailableTimes = this._renderAvailableTimes.bind(this);

    /** Default state */
    this.state = {
      // The date selected on the calendar
      selectedDate: null,
      // Whole parsed date
      parsedSelectedDateTime: null,
      // The datetime to send
      sendSelectedDateTime: null,
      // The current month we are visualizing
      // we save it in the state to minimize the number
      // of requests we make to Google Calendar
      // 0-indexed
      currentMonth: new Date().getMonth(),
      // The availability from the Calendar
      availability: null,
      // Is it loading the availability?
      isLoading: false
    }
  }

  /**
   * Click handler for the calendar
   */
  _handleCalendarChange( date ){
    let previousMonth = this.state.currentMonth;

    this.setState({
      selectedDate: moment(date),
      currentMonth: date.getMonth(),
      parsedSelectedDateTime: null,
      sendSelectedDateTime: null
    }, () => {
      if( previousMonth !== date.getMonth() ){
        this._getMonthAvailability(date);
      }
    });
  }

  /**
   * Click handler for the time radios
   * VERY convoluted way of treating TZ's
   */
  _handleTimeChange(e){
    let selectedDate = moment(this.state.selectedDate).tz('America/Chicago');
    let sendTime = selectedDate
                  .set('hour', e.target.value)
                  .set('minute', 0)
                  .set('second', 0)
                  .set('millisecond', 0)
                  .set('year', moment(this.state.selectedDate).year())
                  .set('month', moment(this.state.selectedDate).month())
                  .set('date', moment(this.state.selectedDate).date());

    this.setState({
      parsedSelectedDateTime: `${moment(selectedDate).format('dddd, MMMM Do YYYY')} at ${this.tConvert(e.target.value)}`,
      sendSelectedDateTime: moment(sendTime).toISOString()
    })
  }

  /**
   * Render available times
   * depending on the selected date
   */
  _renderAvailableTimes(){
    let selectedDay    = this.state.selectedDate.toDate().getDate();
    let selectedMonth  = this.state.selectedDate.toDate().getMonth();
    let availability   = get(this.state.availability.availability, selectedDay, false);
    let empty          = <em>No available times. Please select a different date.</em>;
    
    if( availability !== false ){
      let returnList = keys(availability).map(time => {
        let disabled = !get(this.state.availability.availability, selectedDay + '.' + time, false);
        let timeLabel = this.tConvert(time);
        return (
          <label className={disabled ? 'sh-time-disabled' : ''} key={selectedDay + '-' + selectedMonth + '-' + time}>
            <input name="sh-time" onClick={this._handleTimeChange} type="radio" value={time} disabled={disabled} />
            {timeLabel}
          </label>
        );
      });

      return returnList.length > 0 ? returnList : empty;
    }

    return empty
  }

  /**
   * Convert time format
   */
  tConvert (time) {
    // Check correct time format and split into components  
    let ampm = +time < 12 ? 'am' : 'pm'; // Set AM/PM
    time = +time % 12 || 12; // Adjust hours
    return time + ampm; // return adjusted time or original string
  }

  /**
   * Get the availability from the BE
   * @param {Date} date 
   */
  _getMonthAvailability(date){
    // Set the loading
    this.setState({
      isLoading: true
    }, () => {
      // Make the request
      axios.post(AJAX_URL, qs.stringify({
        action: 'get_calendar_availability',
        date: moment(date).format('YYYY-MM-DD')
      })).then(response => {
        // Set the availability and the loading state
        this.setState({
          isLoading: false,
          availability: response.data.data
        })
      });
    });
  }

  componentDidMount(){
    /**
     * Get the availability from the Calendar
     * of the current month
     */
    this._getMonthAvailability(new Date());
  }

  render() {
    return (
      <div className="sh-calendar">
        <Calendar
          className={`is-loading-${this.state.isLoading}`}
          showNeighboringMonth={false}
          minDate={new Date()}
          minDetail='year'
          onChange={this._handleCalendarChange}
          value={this.state.selectedDate !== null ? this.state.selectedDate.toDate() : null}
        />
        {this.state.selectedDate !== null &&
          <div className={`sh-calendar-available-times is-loading-${this.state.isLoading}`}>
            <p>Plase select a time:</p>
            {this.state.isLoading ? <em>Loading...</em> : this._renderAvailableTimes()}
          </div>
        }
        {this.state.parsedSelectedDateTime !== null && 
        <p className="sh-calendar-selected-date"><strong>Selected date & time:</strong> {this.state.parsedSelectedDateTime}</p>}
        <input type="text" required name="sh-calendar-pretty-datetime" value={this.state.parsedSelectedDateTime || ''} />
        <input type="text" required name="sh-calendar-datetime" value={this.state.sendSelectedDateTime || ''} />
      </div>
    );
  }
}

export default App;
